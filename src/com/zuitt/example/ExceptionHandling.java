package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num = 21;
        System.out.print("Enter a number: ");

        // try - tries to execute the code
        try {
            num = scanner.nextInt();
            System.out.println("Hello " + num);
        }

        // catch - catches the error message
        catch (Exception error){
            System.out.println("Invalid input.");

            // prints the throwable error along with other details like the line number and class name where the exception occurred
            error.printStackTrace();
        }

        // optional block
        // Note that the finally block is particularly useful when dealing with highly sensitive operation, such as
        // ensuring safe operation on databases by closing them if they are no longer needed
        // operations that have unpredictable situations such as if it is possible for users to input problematic info
        // any operation that may be susceptible to abuse and malicious acts, such as downloading or saving files

        finally {
            System.out.println("Hi " + num);
        }
    }
}
