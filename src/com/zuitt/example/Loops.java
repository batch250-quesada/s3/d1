package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class Loops {
    public static void main(String[] args) {

        // while loop - good for inputting values
//        int x = 0;
//        while ( x < 5 ) {
//            System.out.println(x);
//            x++;
//        }

//        int y = 10;
//        do {
//            System.out.println(y);
//            y--;
//        } while ( y > 5 );

        // for loop - most compact

        // for loop with arrays
//        int [] intArray = {1, 2, 3, 4, 5};
//        for (int i = 0; i < intArray.length; i++) {
//            System.out.print(intArray[i]);
//        }

        // for-each loop with arrays
        // for(dataType itemName: arrayName) {code block}

//        String [] names = {"john", "paul", "george", "ringo"};
//        for (String name : names) {
//            System.out.println(name.toUpperCase());
//            if (name == "paul") return;
//        }

        // nested for-loop
        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";
//
//        for (int row = 0; row < 3; row++) {
//            for ( int column = 0; column < 3; column++) {
//                System.out.println("classroom[" + row + "][" + column + "] = " + classroom[row][column]);
//            }
//        }

        // for-each loop with arraylist
        // arrayList.forEach(Consumer<E> -> // code block);
        // -> is called the lambda operator which is used t separate the parameter and implementation/expression

//        ArrayList<Integer> numbers = new ArrayList<>();
//        numbers.add(1);
//        numbers.add(2);
//        numbers.add(3);
//        numbers.add(4);
//        numbers.add(5);
//        System.out.println("@numbers" + numbers);
//
//        numbers.forEach(number -> System.out.println("@number " + number));

        // for-each with hashmaps
        // hashmapName.forEach((key, value) -> // code block);
//        HashMap<String, Integer> grades = new HashMap<String, Integer>();
//        grades.put("Arts", 90);
//        grades.put("Math", 91);
//        grades.put("Science", 93);
//        grades.put("Music", 94);
//
//        grades.forEach((subject, grade) -> System.out.println(subject + ": " + grade));


        // for-each loop with multidimensional array
        // from above example
        // accessing each row
        for (String [] row : classroom) {
            // accessing each column (actual element)
            for(String column : row) {
                System.out.println(column);
            }
        }
    }
}
